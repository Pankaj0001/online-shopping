<?php
include 'db/conn.php';
require_once("views/header.php");
//session_start();
?> <body bgcolor="gray"> <?php
if(!isset($_SESSION['username']))
    {
        echo '<center><h2> You need to Login First</h2></center>';
    
    }else{

    
$sql = "SELECT product.*,cart.id as cart_id from product inner join cart on cart.product_id = product.id where cart.user_id = " . $_SESSION['user_id'] . " and cart.status = 1";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
?>
    
    <head>
        <style>
            

            input[type=text],
            select {
                width: 90%;
                padding: 12px 20px;
                margin: 20px 10px;
                display: inline-block;
                border: 3px solid #ccc;
                border-radius: 4px;
                color: white;
                box-sizing: border-box;
                background-color: gray;
            }

/*
            table {
                border-collapse: collapse;
                width: 100%;
                margin-top:50px;
            }

            th,
            td {
                 text-align: left;
            padding: 8px; 
                text-align: center;
                vertical-align: middle;
            }

            tr:nth-child(even) {
                background-color: #f2f2f2
            }

            th {
                background-color: Black;
                color: white;
            }
*/
            
             #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td,
        #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #customers tr:hover {
            /* background-color: #ddd; */
        }

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: black;
            color: white;
        }
            
            
            .button {
                background-color: #4CAF50;                
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                margin-top:20px;
                border-style: 3px solid;
                border-color: black;
                
            }
        </style>        
    </head>
    <div id='old_table'>
        <table border='1'  id='customers'>
            <tr>
                <th>Image </th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Like Count</th>
                <th>Action</th>

            </tr>

            <?php
            while ($row = $result->fetch_assoc()) {
            ?>
                <tr>
                    <td><img src='<?= $row["image_url"] ?>'  width="400px" height="200px" style="margin:10px;"></td>
                    <td><?= $row["name"] ?></td>
                    <td><?= $row["price"]; ?></td>
                    <td><?= $row["quantity"]; ?></td> 
                    <td><?= $row["like_count"]; ?></td>
                    <td><a href="delete_from_cart.php?id=<?= $row["cart_id"] ?>"><input type="button" value='Delete From Cart'></a></td>
                </tr>

            <?php
            }
            ?>
        </table>

        <center><a href="placeorder.php"><input type="button" value="place order" name='submit' class="button"></a></center>
    </div>
<?php
} else {
    echo '<h2>Your cart is Empty</h2>';
}

    }
?>