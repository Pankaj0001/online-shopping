<?php
include 'db/conn.php';
require_once("views/header.php");
//session_start();

$query_condition = "";

if (isset($_GET['category'])) {
    $query_condition = " and product.category = " . $_GET['category'];
}

$sql = "SELECT product.*,category.name as c_name from product inner join category on product.category = category.id where product.status = 1" . $query_condition;
$result = $conn->query($sql);

?>

<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <style>
        .button {
            background-color: #4CAF50;

            /* Green */
            border: none;
            color: white;
            padding: 15px 70px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            border-style: solid;
            border-color: black;
            border-radius: 50%;
            margin-right: 30px;
            margin-left: 70px;
            height: 200px;
            width: 200px;
        }

        .button2 {
            background-color: #008CBA;
        }

        .button3 {
            background-color: #f44336;
        }

        .button4 {
            background-color: #e7e7e7;
            color: black;
        }

        input[type=text],
        select {
            width: 90%;
            padding: 12px 20px;
            margin: 20px 10px;
            display: inline-block;
            border: 3px solid #ccc;
            border-radius: 4px;
            color: white;
            box-sizing: border-box;
            background-color: gray;
        }

        /* table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {

            text-align: center;
            vertical-align: middle;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }

        th {
            background-color: Black;
            color: white;
        } */

        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td,
        #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #customers tr:hover {
            /* background-color: #ddd; */
        }

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: black;
            color: white;
        }




        .column {
            float: left;
            width: 25%;
            padding: 5px;
        }

        /* Clearfix (clear floats) */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }

        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 26px;
        }

        .container {
            position: relative;
            text-align: center;
            color: white;
            /* border-style: solid; */
            /* border-color:black; */
            border-radius: 50%;
        }

        * {
            box-sizing: border-box;
        }

        .container img {
            border-radius: 70px;
            border-style: solid;
            border-color: black;

        }

        a:hover img {
            /* width: 26%; */
            transform: scale(1.05);
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
        }
        td:hover img{
            transform: scale(1.05);
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
        }
    </style>
</head>
<hr>
<center><b>Select Category</b></center><br>
<!-- <div class='row'>
    <a href='product.php?category=1'><input type='button' value="Electronics" class='button'></a>
    <a href='product.php?category=2'><input type='button' value="Clothing" class='button button2'></a>
    <a href='product.php?category=3'><input type='button' value="Footwear" class='button button3'></a>
    <a href='product.php?category=4'><input type='button' value="Jwelery" class='button button4'></a>
</div> -->


<div class="row">
    <div class="column">
        <div class="container">
            <a href="product.php?category=3">
                <img src="./image/brown.jpg" alt="Snow" style="width:100%">
            </a>
            <div class="centered">Footware</div>
        </div>
    </div>
    <div class="column">
        <div class="container">
            <a href="product.php?category=4">
                <img src="./image/jewellery.jpg" alt="Forest" style="width:100%">
            </a>
            <div class="centered">Jewellery</div>
        </div>
    </div>
    <div class="column">
        <div class="container">
            <a href="product.php?category=2">
                <img src="./image/tie.jpg" alt="Forest" style="width:100%">
            </a>
            <div class="centered">Clothing</div>
        </div>
    </div>
    <div class="column">
        <div class="container">
            <a href="product.php?category=1">
                <img src="./image/apple.jpg" alt="Mountains" style="width:100%">
            </a>
            <div class="centered">Electronics</div>
        </div>
    </div>
</div>



<hr>

<b>Search</b>
<input type='text' id="search_id" placeholder="Search apply after 2 characters" oninput="searchFcuntion()">

<?php

if ($result->num_rows > 0) {
    // output data of each row
?>

    <body bgcolor="gray">

        <div id="old_table">
            <table border='1' id="customers">
                <tr>
                    <th>Image </th>
                    <th>Name</th>
                    <th>Price</th>
                    <!-- <th>Quantity</th> -->
                    <th>Like Count</th>
                    <th>Category</th>
                    <?php if (isset($_SESSION['user_id'])){ ?>
                    <th>Action</th>
                    <?php } ?>
                </tr>

                <?php
                while ($row = $result->fetch_assoc()) {
                ?>
                    <tr>
                        <td><img src='<?= $row["image_url"] ?>' width="400px" height="200px" style="margin:10px;"></td>
                        <td><b><?= $row["name"] ?></b></td>
                        <td><?= $row["price"]; ?></td>
                        <!-- <td><?php // $row["quantity"]; ?></td> -->
                        <td><?= $row["like_count"]; ?></td>
                        <td><?= $row["c_name"]; ?></td>
                        <?php if (isset($_SESSION['user_id'])){ ?>
                        <td><a href="likepage.php?id=<?= $row["id"] ?>"><input type='button' value="Like product" height="20px" ; width="15px"> </a>
                           <?php if (isset($_SESSION['user_id']) && ($_SESSION['user_id'] != 2)) { ?>
                            <a href="add_to_cart.php?id=<?= $row["id"] ?>"> <input type='button' value="Add To Cart" height="20px" ; width="15px"></a>
                           <?php } ?>
                            <?php
                            if (isset($_SESSION['user_id']) && ($_SESSION['user_id'] == 2)) { ?>
                                <a href="delete_product.php?id=<?= $row["id"] ?>"> <input type='button' value="Delete Product" height="20px" ; width="15px"></a>
                            <?php  }
                            ?>

                        </td>
                        <?php } ?>

                    </tr>
                <?php
                }
                ?>
            </table>
        </div>
        <div id="new_table">

        </div>
        <div id='emptyy'>
        </div>
    </body>
<?php
} else {
    echo 'no product to display';
}
require_once("views/footer.php");


?>

<script>
    function searchFcuntion() {

        var str = document.getElementById("search_id").value;
        if (str.length > 2) {
            $.ajax({
                url: "dataa.php",
                type: "get",
                data: {
                    "str": str
                },
                success: function(result) {
                    result = JSON.parse(result);
                    // if (result && result.length) {
                    $('#old_table').hide();
                    $('#new_table').show();
                    // } else {
                    //     $('#old_table').show();
                    //     $('#new_table').hide();
                    // }
                    var html = "<table border='1' class='scroll' id='customers'>";
                    html += "<tr>";
                    html += "<th> image_url </th>";
                    html += "<th> name </th>";
                    html += "<th> price </th>";
                  //  html += "<th> quantity </th>";
                    html += "<th> like_count </th>";
                    html += "<th> Action </th>";
                    html += "</tr>";
                    // console.log(result['0']);
                    for (let i = 0; i < result.length; i++) {
                        console.log(result[i]);
                        html += "<tr>";
                        html += "<td> <img src = '" + result[i]['image_url'] + "'width='400px' height='200px' style='margin:10px;'></td>";
                        html += "<td><b>" + result[i]['name'] + "</b></td>";
                        html += "<td>" + result[i]['price'] + "</td>";
                       // html += "<td>" + result[i]['quantity'] + "</td>";
                        html += "<td>" + result[i]['like_count'] + "</td>";
                        html += "<td><a href='likepage.php?id=" + result[i]["id"] + "'><input type='button' value='Like Product' height='20px' ; width='15px'></a><a href='add_to_cart.php?id=" + result[i]["id"] + "'> <input type='button' value='Add To Cart' height='20px' ; width='15px'></a></td>";
                        // html += "<td><a href='add_to_cart.php?id=" + result[i]["id"] + "'> <input type='button' value='Add To Cart' height='20px' ; width='15px'></a></td>";
                        html += "</tr>";
                    }
                    html += "</table>";
                    document.getElementById("new_table").innerHTML = html;
                    if (result && result.length == 0) {
                        var htmll = "<div style='color:white;'><center><h2>No Product to Display</h2></center></div>"
                        document.getElementById("emptyy").innerHTML = htmll;
                    } else {
                        document.getElementById("emptyy").innerHTML = '';
                    }
                    //console.log(result);
                }
            });
        } else {
            $('#old_table').show();
            $('#new_table').hide();
            document.getElementById("emptyy").innerHTML = '';
            return;
        }

    }
</script>