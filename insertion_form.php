<?php
//session_start();
include 'db/conn.php';
require_once("views/header.php");

if ($conn->connect_error) {
    die("connection failed : " . $conn->connect_error);
}
if (isset($_POST['logout'])) {
    unset($_SESSION['username']);
    header('location:index.php');
    //session_start();
}else{
   // header('location:login.php');
}
$sql = "Select * from category";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) 
    {
?>

<head>
    <style>
        input[type=text],
        input[type=number],
        input[type=file],        
        select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=password],
        select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        input[type=submit]:hover {
            background-color: #45a049;
        }

    </style>
</head>
<body bgcolor="gray">
<?php
if ($_SESSION['username'] == true) {
?>
    
    <form action="db/insertion.php" method="post" enctype="multipart/form-data">
        Name:<br>
        <input type="text" name="name" value="" pattern="[a-zA-z]{1,25}"><br>
        Price:<br>
        <input type="number" name="price" value=""><br><br>
        Quantity:<br>
        <input type="number" name="quantity" value=""><br><br>
        Description:<br>
        <input type="text" name="description" value=""><br><br>
         Category :<br>
        <select name="category">
           <?php
        while($row = $result->fetch_assoc()) 
        {

?>
            <option value="<?= $row["id"]  ?>"> <?= $row["name"]  ?> </option>
          <?php
        }
}
            ?>
     
        </select>
        Showroom Product : <br>
        <input type="radio" name="is_showroom" value="1" checked> Yes
        <input type="radio" name="is_showroom" value="0"> NO<br>
        Image:<br>
        <input type="file" name="image" multiple='multiple'><br><br>

        <input type="submit" value="Submit">
    </form>
<?php
} 
?>