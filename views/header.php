<?php
session_start();

?>
<html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <style>
        /* body {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
        } */

        /* .topnav {
            overflow: hidden;
            background-color: #333;
            height: 70px;            
        }
        
        .topnav a {
            float: left;
            color: #f2f2f2;
            text-align: center;
            margin-top : 9px;
            padding: 16px 16px;
            text-decoration: none;
            font-size: 17px;
        }
        
        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }
        
        .topnav a.active {
            background-color: gray;
            color: white;
        }
        .footer {
        position: relative;    
        left: 0;
        bottom: 0;
        height: 100px;
        width: 100%;
        background-color: rgba(20, 25, 96, 0.85);
        color: white;
        text-align: center; 
   }*/

        /* new css */
        body {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
        }

        .topnav {
            overflow: hidden;
            background-color: #333;
            /* height: 80px; */
        }

        .topnav a {
            float: left;
            display: block;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 20px;
            /* padding-top: 30px; */
            padding-left: 70px;
            padding-right: 70px;
            /* height: 50px; */
        }

        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        .topnav a.active {
            background-color: black;
            color: white;
        }

        .topnav .icon {
            display: none;
        }

        @media screen and (max-width: 600px) {
            .topnav a:not(:first-child) {
                display: none;
            }

            .topnav a.icon {
                float: right;
                display: block;
            }
        }

        @media screen and (max-width: 600px) {
            .topnav.responsive {
                position: relative;
            }

            .topnav.responsive .icon {
                position: absolute;
                right: 0;
                top: 0;
            }

            .topnav.responsive a {
                float: none;
                display: block;
                text-align: left;
            }
        }
    </style>

    <title>Document</title>

</head>

<!-- <header> -->
    <center>
    <div style="float:left;margin-top:-15px"><img src="./image/mylogo.png" height="70px" width="250px"></div>
    
<?php if (isset($_SESSION['username'])) { ?>
    <h1>WELCOME <?= $_SESSION['username'] ?></h1>
<?php } else { ?>
    <h1>SHOPPER'S WORLD</h1>
<?php } ?>
    
    </center>
    <!-- <div style="float:right;margin-top:-75px"><img src="./image/logo1.png" height="70px" width="250px"></div> -->

<!-- </header> -->

<body>

    <div class="topnav" id="myTopnav">
        <a class="active" href="index.php">Home</a>
        <a href="product.php">Catalog</a>
        <a href="cart.php">Cart</a>
        <?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == true) { ?>
            <a href="logout.php"> Log Out </a>
        <?php } else { ?>
            <a href="login.php"> Log in </a>
            <a href="signup.php">SignUp</a>
        <?php } ?>
        <?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == 2) { ?>
            <a href="adminview.php"> AdminView </a>
        <?php }  ?>
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
            <i class="fa fa-bars"></i>
        </a>
    </div>

    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>

</body>

</html>