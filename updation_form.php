<?php
include 'db/conn.php';
require_once("views/header.php");

$sql = "SELECT * from product where id=" . $_GET['id'];
$result = $conn->query($sql);



while ($row = $result->fetch_assoc()) {
?>

    <head>
        <style>
            input[type=text],
            input[type=number],
            input[type=file],
            select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=password],
            select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }
        </style>
    </head>

    <body bgcolor="gray"> 
       <form action="db_update.php" method="post" enctype="multipart/form-data">
        Name:<br>
        <input type="text" name="name" value="<?= $row['name'] ?>" pattern="[a-zA-z]{1,25}"><br>
        <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
        Price:<br>
        <input type="number" name="price" value="<?= $row['price'] ?>"><br><br>
        Quantity:<br>
        <input type="number" name="quantity" value="<?= $row['quantity'] ?>"><br><br>
        Description:<br>
        <input type="text" name="description" value="<?= $row['description'] ?>"><br><br>
        Showroom Product : <br>
        <input type="radio" name="is_showroom" value="1" checked> Yes
        <input type="radio" name="is_showroom" value="0"> NO<br><br>
        Category : 
        <select name="category">
           <?php
    $sql1 = "Select * from category";
    $result1 = $conn->query($sql1);

    if ($result1->num_rows > 0) 
    {
        while($row1 = $result1->fetch_assoc()) 
        {
            if($row['category'] == $row1["id"]){ ?>
                <option value="<?= $row1["id"]  ?>" selected> <?= $row1["name"]  ?> </option>
           <?php }else{ ?>
                   
         <option value="<?= $row1["id"]  ?>"> <?= $row1["name"]  ?> </option>              
<?php                    
            }
?>        
            
          <?php
        }
}
            ?>
           </select>
            <label>Image:</label><br>
        <input type="file" name="image" ><br><br>
        <input type="hidden" name="file" value="<?= $row['image_url'] ?>">
        
        <input type="submit" value="Submit">
        </form>
 <?php
        }


            ?>