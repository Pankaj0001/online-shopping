-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 01, 2020 at 08:34 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `choclatefactory`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `status`) VALUES
(1, 2, 14, 0),
(2, 2, 14, 0),
(3, 2, 38, 0),
(4, 11, 34, 0),
(5, 12, 33, 0),
(6, 7, 31, 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `created_at`) VALUES
(1, 'Electronics', '2020-08-23 12:17:16'),
(2, 'Clothing', '2020-08-23 12:17:16'),
(3, 'Footwear', '2020-08-23 12:17:16'),
(4, 'Jwelery', '2020-08-23 12:17:16');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `image_name` varchar(100) DEFAULT NULL,
  `image_url` varchar(100) NOT NULL,
  `like_count` int(11) DEFAULT '0',
  `description` varchar(500) NOT NULL,
  `is_showroom` int(1) NOT NULL DEFAULT '0',
  `category` int(11) DEFAULT '1',
  `status` int(1) DEFAULT '1' COMMENT '0->inactive,1->active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `quantity`, `price`, `image_name`, `image_url`, `like_count`, `description`, `is_showroom`, `category`, `status`) VALUES
(14, 'darkchoclate', 3, 5, NULL, 'image/3.jpg', 8, 'bit bitter', 1, 2, 0),
(15, 'choclatetrufle', 3, 5, NULL, 'image/4.jpg', 0, 'delicious', 0, 2, 0),
(16, 'choclatetrufle', 3, 5, NULL, 'image/4.jpg', 0, 'delicious', 0, 2, 0),
(17, 'choclate', 2, 20, NULL, 'image/1.jpg', 0, 'good', 0, 2, 0),
(19, 'munch', 3, 1, NULL, 'image/home.jpg', 0, 'delicious', 1, 1, 0),
(20, 'candy', 3, 2, NULL, 'image/home.jpg', 0, 'very tasty', 1, 1, 0),
(21, 'candy', 3, 2, NULL, 'image/home.jpg', 0, 'very tasty', 1, 1, 0),
(22, 'candy', 1, 98, NULL, 'image/3.jpg', 0, 'delicious', 1, 1, 0),
(23, 'munch', 2, 90, NULL, 'image/2.jpg', 0, 'very tasty', 0, 1, 0),
(24, '', 1, 90, NULL, 'image/2.jpg', 0, 'very tasty', 1, 1, 0),
(25, 'tt', 3, 2, NULL, 'image/blueheels.jpg', 0, 'High quality sunglasses,with Full UV protection ', 1, 1, 0),
(26, 'Earrings', 1, 120, NULL, 'image/earing.jpg', 0, 'Oxidized ear-ring with high quality material, suitable for daily wear as well parties.', 1, 4, 0),
(27, 'Earrings', 1, 120, NULL, 'image/stering sliver.jpg', 0, 'Party style earring in blue color stone, also available in multiple colors.', 1, 4, 0),
(28, 'Ring', 1, 350, NULL, '', 0, 'Ring perfect for party style or can be wear in regular routine.', 1, 1, 0),
(29, 'Tshirt', 1, 65, NULL, 'image/brown_tshirt.jpg', 0, 'Casual T-shirt in wrinkle free fabric. Available in all sizes', 1, 2, 1),
(30, 'Shirt', 1, 120, NULL, 'image/white shirt.jpg', 0, 'Formal shirt , available in all sizes and two colors-white and black', 1, 2, 1),
(31, 'Earrings', 1, 150, NULL, 'image/stering sliver.jpg', 0, 'Party style earring in blue color stone, also available in multiple colors.', 1, 4, 1),
(32, 'Ring', 1, 400, NULL, '', 0, '18k Gold ring ,Perfect party style', 1, 4, 0),
(33, 'Ring', 1, 450, NULL, 'image/ring.jpg', 0, '18k Gold ring ,Perfect for party style', 1, 4, 1),
(34, 'Shoes', 1, 150, NULL, 'image/Red_shoes.jpg', 0, 'Running shoes with memory form, very soft and comfortable', 1, 3, 1),
(35, 'Shoes', 1, 249, NULL, 'image/red.jpg', 0, 'Running shoes,available in two more colors red and blue', 1, 3, 1),
(36, 'Dress', 1, 180, NULL, 'image/frock.jpg', 0, 'Beautiful denim  dress available in small , medium and large size.', 1, 2, 1),
(37, 'Dress', 1, 150, NULL, 'image/top.jpg', 0, 'Fancy dress , can be casual wear or party wear.', 1, 2, 1),
(38, 'Microwave', 1, 299, NULL, 'image/microwave.jpg', 0, 'Microwave with low power consumption.', 1, 1, 1),
(39, 'Camera', 1, 400, NULL, 'image/camera.jpg', 0, 'Camera with auto zoom and focus features. ', 1, 1, 1),
(40, 'Lamp', 1, 200, NULL, 'image/lamps.jpg', 0, 'Modern decor style with the beautiful 3 lamp set for adding style to your home', 1, 1, 1),
(41, 'Lamp', 1, 90, NULL, 'image/lamp2.jpg', 0, 'Table lamp, suitable for bed side', 1, 1, 1),
(42, 'Shoes', 1, 299, NULL, 'image/shoe.jpg', 0, 'Shoes suitable for casual wear.', 1, 3, 1),
(43, 'Shirt', 1, 199, NULL, 'image/white.jpg', 0, 'Formal shirt for girls, perfect for office style ', 1, 2, 1),
(44, 'Television', 1, 499, NULL, 'image/Tv1.jpg', 0, 'LED tv with good quality display and HD view, high resoluation ', 1, 1, 1),
(45, 'Earrings', 1, 75, NULL, 'image/ear.jpg', 0, 'Gold platted earring ,elegant and stylish good for party wear', 1, 4, 1),
(46, 'Necklace', 1, 299, NULL, 'image/necklace.jpg', 0, 'Elegant pearl necklace', 1, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `question` varchar(250) DEFAULT NULL,
  `answer` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `question`, `answer`, `created_at`) VALUES
(2, 'pritpal', 'prit@mylofamily.com', 'Prit#87gat', '2', 'red', '2020-08-22 23:25:28'),
(3, 'pankaj', 'pankaj@mylofamily.com', 'Pnakj12@#$hga', '1', 'gurjeet', '2020-08-22 23:36:12'),
(4, 'pritpal', 'Pfitpal@mylofamily.com', 'Pfrit@987654', '1', 'bbb', '2020-08-22 23:40:22'),
(5, 'Parndeep kaur', 'kaurparndeep777@gmail.com', 'Imhappy@01', '1', 'flocky', '2020-08-31 20:32:47'),
(6, 'rajdeep', 'rajdeep.singh1996@gmail.com', 'Rajdeep@1996', '2', 'Black', '2020-09-01 14:54:02'),
(7, 'Rajwinder', 'rajwindersandhu93@gmail.com', 'Rajwinder@93', '3', 'Parn', '2020-09-01 14:55:32'),
(8, 'Karanveer', 'kveer396@gmail.com', 'Karan@396', '1', 'Doggy', '2020-09-01 14:57:16'),
(9, 'Kiran', 'kiranlubana27@gmail.com', 'Kiran@27', '1', 'Doggy', '2020-09-01 14:58:39'),
(10, 'Parndeep kaur', 'parndeepkaur19@gmail.com', 'Imhappy@29', '1', 'flocky', '2020-09-01 15:15:39'),
(11, 'Parndeep kaur', 'parndeepkaur19@gmail.com', 'Imhappy@29', '1', 'flocky', '2020-09-01 15:16:25'),
(12, 'minnie', 'kaurparandeep777@gmail.com', 'Imhappy@29', '1', 'flocky', '2020-09-01 15:18:49');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
