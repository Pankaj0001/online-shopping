<?php
//session_destroy();
include 'views/header.php';
//session_start();

?>

<head>
    <style>
        @keyframes slidy {
            0% {
                left: 0%;
            }

            20% {
                left: 0%;
            }

            25% {
                left: -100%;
            }

            45% {
                left: -100%;
            }

            50% {
                left: -200%;
            }

            70% {
                left: -200%;
            }

            75% {
                left: -300%;
            }

            95% {
                left: -300%;
            }

            100% {
                left: -400%;
            }
        }

        body {
            margin: 0;
        }

        div#slider {
            overflow: hidden;
        }

        div#slider figure img {
            width: 20%;
            height: 100%;
            float: left;
        }

        div#slider figure {
            position: relative;
            width: 500%;
            height: 700px;
            margin: 0;
            left: 0;
            text-align: left;
            font-size: 0;
            animation: 30s slidy infinite;
        }

        /* .block {
            height: 30%;
            width: 40%;
            margin: 10px;
        }
        */

        /* .category_container {
            height: 800px;
            width: 80%;
        } */

        * {
            box-sizing: border-box;
        }

        .column {
            float: left;
            width: 25%;
            padding: 5px;
        }

        /* Clearfix (clear floats) */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }

        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 26px;            
        }

        .container {
            position: relative;
            text-align: center;
            color: white;
            border-style: solid;
            border-color:black;
        }

        a:hover img {
            /* width: 26%; */
            transform: scale(1.05);
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
        }
        
    </style>
</head>

<body bgcolor="gray">


    <div>
        <center><h3>Let's shop with us....</h3></center>
        <p>Shopper's World!!!! shopper's peradice for shopping all daily needed articles at one place.Here you can shop clothing,electonic,footwears and jewellary.Now shopping is as simple as single click. </p>
    </div>

    <div class="category_container">

        <!-- here we will show category design -->
        <div class="row">
            <div class="column">
                <div class="container">
                    <a href="product.php?category=3">
                        <img src="./image/brown.jpg" alt="Snow" style="width:100%">
                    </a>
                    <div class="centered">Footware</div>
                </div>
            </div>
            <div class="column">
                <div class="container">
                    <a href="product.php?category=4">
                        <img src="./image/jewellery.jpg" alt="Forest" style="width:100%">
                    </a>
                    <div class="centered">Jewellery</div>
                </div>
            </div>
            <div class="column">
                <div class="container">
                    <a href="product.php?category=2">
                        <img src="./image/tie.jpg" alt="Forest" style="width:100%">
                    </a>
                    <div class="centered">Clothing</div>
                </div>
            </div>
            <div class="column">
                <div class="container">
                    <a href="product.php?category=1">
                        <img src="./image/apple.jpg" alt="Mountains" style="width:100%">
                    </a>
                    <div class="centered">Electronics</div>
                </div>
            </div>
        </div>
    </div>
    <div id="slider">
        <figure>
            <img src="image/fashion1.jpg" alt>
            <img src="image/fasion1.jpg" alt>
            <img src="image/brand.jpg" alt>
            <img src="image/brand1.jpg" alt>
            <img src="image/shoe1.jpg" alt>
        </figure>
    </div>
</body>
<?php


include 'views/footer.php';
?>