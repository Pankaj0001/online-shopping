<?php

require_once("views/header.php");
require_once("db/conn.php");
$sql = "SELECT * from product";
$result = $conn->query($sql);

//session_start();
if ($_SESSION['username'] == true) {

    if ($result->num_rows > 0) {
?>

        <head>
            <style>
                .button {
                    background-color: #4CAF50;
                    /* Green */
                    border: none;
                    color: white;
                    padding: 15px 70px;
                    text-align: center;
                    text-decoration: none;
                    display: inline-block;
                    font-size: 16px;
                    margin: 4px 2px;
                    cursor: pointer;
                    border-style: solid;
                    border-color: black;
                    border-radius: 45%;
                }

                .button2 {
                    background-color: #008CBA;
                }

                .button3 {
                    background-color: #f44336;
                }

                .button4 {
                    background-color: #e7e7e7;
                    color: black;
                }

                input[type=text],
                select {
                    width: 90%;
                    padding: 12px 20px;
                    margin: 20px 10px;
                    display: inline-block;
                    border: 3px solid #ccc;
                    border-radius: 4px;
                    color: white;
                    box-sizing: border-box;
                    background-color: gray;
                }

                table {
                    border-collapse: collapse;
                    width: 100%;
                }

                th,
                td {
                    /* text-align: left;
                padding: 8px; */
                    text-align: center;
                    vertical-align: middle;
                }

                tr:nth-child(even) {
                    background-color: #f2f2f2
                }

                th {
                    background-color: Black;
                    color: white;
                }
            </style>
        </head>
                <body bgcolor="gray">
        <table border='1'>
            <tr>
                <th>Image </th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Like Count</th>                
                <th>Action</th>
            </tr>

        <ul>
            <?php
            while ($row = $result->fetch_assoc()) {

            ?>
                <tr>
                    <td><img src="<?= $row["image_url"]  ?>" height="300" width="400"></td>
                    <td><?= $row["name"]; ?></td>
                    <td><?= $row["price"]; ?></td>
                    <td><?= $row["quantity"]; ?></td>
                    <td><?= $row["like_count"]; ?></td>
                    <td><a href="updation_form.php?id=<?= $row["id"] ?>">Update Product</a></td>
                </tr>

               
            <?php
            }

            ?>

        </ul>

        </table>
        </body>

<?php
    } else {
        echo 'no product to display';
    }
} else
    header('location:login.php');
?>